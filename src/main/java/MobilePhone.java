import java.util.ArrayList;

public class MobilePhone
{
    private ArrayList<Contact> myContacts;
    private String myNumber;

    public MobilePhone(String myNumber)
    {
        this.myNumber = myNumber;
        this.myContacts = new ArrayList<Contact>();
    }

    public void printContacts()
    {
        for(Contact contact : myContacts)
        {
            System.out.println(contact.getName() + "->" + contact.getPhoneNumber());
        }
    }

    public boolean addNewContact(Contact contact)
    {
        if(findContact(contact.getName()) >= 0)
        {
            System.out.println("Contact already exists");
            return false;
        }
        myContacts.add(contact);
        return true;
    }

    public boolean updateContact(Contact oldContact, Contact newContact)
    {
        int foundPosition = findContact(oldContact);
        if(foundPosition < 0)
        {
            System.out.println(oldContact.getName() + " was not found");
            return false;
        }
        //If the newContact already exists in the contacts array
        //Print error and return false, otherwise we can change
        if((findContact(newContact.getName()) != -1))
        {
            this.myContacts.set(foundPosition, newContact);
            System.out.println(oldContact.getName() + " was replaced with " + newContact.getName());
            return true;
        }
        System.out.println("Can't change name to one that is already in the list");
        return false;
    }

    public boolean removeContact(Contact contact)
    {
        int foundPosition = findContact(contact);
        if(foundPosition < 0)
        {
            System.out.println(contact.getName() + " does not exist");
            return false;
        }
        this.myContacts.remove(foundPosition);
        return true;
    }

    public String queryContact(Contact contact)
    {
        if(findContact(contact) >= 0)
        {
            return contact.getName();
        }
        return null;
    }

    public Contact queryContact(String name)
    {
        int position = findContact(name);
        if(position >=0)
        {
            return this.myContacts.get(position);
        }
        return null;
    }

    private int findContact(Contact contact)
    {
        return this.myContacts.indexOf(contact);
    }

    private int findContact(String contactName)
    {
        for(int i=0; i < myContacts.size(); ++i)
        {
            Contact contact = this.myContacts.get(i);
            if(contact.getName().equals(contactName))
            {
                return i;
            }
        }
        return -1;
    }
}
