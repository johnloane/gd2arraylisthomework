import java.util.Scanner;

/* Create a program that implements a mobile phone.
The phone can store, modify, remove and query contact names.
You need to create separate classes for Contact(name, phonenumber)
Also create a MobilePhone class holds an ArrayList of Contacts
Add a menu of options that are available
Options: Quit, print list of contacts, add new contact, update existing contact, remove contact and search contacts.
When adding or updating be sure to check if the contact already exists (use name)
Be sure not not to expose the inner workings of the ArrayList to MobilePhone
MobilePhone should do everything with contact objects only.
 */
public class Main
{
    private static Scanner sc = new Scanner(System.in);
    private static MobilePhone mobilePhone = new MobilePhone("087 1234567");

    public static void main(String[] args)
    {
        boolean quit = false;
        startPhone();
        printActions();
        while(quit == false)
        {
            System.out.print("\nEnter action: (6 to show available actions)> ");
            int action = sc.nextInt();
            sc.nextLine();

            switch(action)
            {
                case 0:
                    System.out.println("Shutting down");
                    quit = true;
                    break;

                case 1:
                    mobilePhone.printContacts();
                    break;
                case 2:
                    addNewContact();
                    break;
                case 3:
                    updateContact();
                    break;
                case 4:
                    removeContact();
                    break;
                case 5:
                    queryContact();
                    break;
                case 6:
                    printActions();
                    break;
            }

        }
    }

    private static void updateContact()
    {
        System.out.println("Enter old contact name: ");
        String name = sc.nextLine();
        Contact existingContactRecord = mobilePhone.queryContact(name);
        if(existingContactRecord == null)
        {
            System.out.println("Contact not found");
            return;
        }
        System.out.println("Enter the new contact name: ");
        String newName = sc.nextLine();
        System.out.println("Enter new contact number: ");
        String newNumber = sc.nextLine();
        Contact newContact = Contact.createContact(newName, newNumber);
        if(mobilePhone.updateContact(existingContactRecord, newContact))
        {
            System.out.println("Successful update");
        }
        else
        {
            System.out.println("Error updating record");
        }
    }

    private static void addNewContact()
    {
        System.out.println("Enter new contact name: ");
        String name = sc.nextLine();
        System.out.println("Enter the phone number: ");
        String phone = sc.nextLine();
        Contact newContact = Contact.createContact(name, phone);
        if(mobilePhone.addNewContact(newContact))
        {
            System.out.println("New contact added: name " + name + ", phone = " + phone);
        }
        else
        {
            System.out.println("Cannot add as it already exists");
        }
    }

    private static void removeContact() {
        System.out.println("Enter existing contact name: ");
        String name = sc.nextLine();
        Contact existingContactRecord = mobilePhone.queryContact(name);
        if (existingContactRecord == null)
        {
            System.out.println("Contact not found.");
            return;
        }

        if(mobilePhone.removeContact(existingContactRecord))
        {
            System.out.println("Successfully deleted");
        }
        else {
            System.out.println("Error deleting contact");
        }
    }

    private static void queryContact() {
        System.out.println("Enter existing contact name: ");
        String name = sc.nextLine();
        Contact existingContactRecord = mobilePhone.queryContact(name);
        if (existingContactRecord == null)
        {
            System.out.println("Contact not found.");
            return;
        }

        System.out.println("Name: " + existingContactRecord.getName() + " phone number is " + existingContactRecord.getPhoneNumber());
    }

    private static void startPhone()
    {
        System.out.println("Starting phone...");
    }

    private static void printActions()
    {
        System.out.println("\nAvailable options:\npress");
        System.out.println("0 - to shutdown\n"+
                           "1 - to print contacts\n"+
                           "2 - to add a new contact\n"+
                           "3 - to update existing contact\n"+
                           "4 - to remove existing contact\n"+
                           "5 - query whether contact exists\n"+
                           "6 - to print list of options");
    }

}
